﻿using Entities.Enuns;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Entities
{
    public class Item
    {
        public int ItemId { get; set; }
        public string Nome { get; set; }
        public int PrazoValidade { get; set; }
        public int Qualidade { get; set; }
        public TipoItem TipoItem { get; set; }
        [ForeignKey("Mercearia")]
        public int MerceariaId { get; set; }
        public virtual Mercearia Mercearia { get; set; }
    }
}
