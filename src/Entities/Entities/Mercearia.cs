﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Entities
{
    public class Mercearia
    {
        public int MerceariaId { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<Item> Itens { get; set; }
    }
}
