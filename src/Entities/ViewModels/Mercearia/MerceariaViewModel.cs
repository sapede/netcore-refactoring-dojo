﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities.ViewModels
{
    public class MerceariaViewModel
    {
        public int MerceariaId { get; set; }
        public string Nome { get; set; }
    }
}
