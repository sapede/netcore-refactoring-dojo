﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities.ViewModels
{
    public class ItemViewModel
    {
        public int ItemId { get; set; }
        public string Nome { get; set; }
        public int PrazoValidade { get; set; }
        public int Qualidade { get; set; }
        public int MerceariaId { get; set; }

    }
}
