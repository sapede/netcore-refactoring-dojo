﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Enuns
{
    public enum TipoItem
    {
        [Display(Name ="Lendário")]
        Lendario = 10,

        [Display(Name = "Normal")]
        Normal = 20,

        [Display(Name = "Ingressos")]
        Ingressos = 30,

        [Display(Name = "Conjurados")]
        Conjurados = 40,

        [Display(Name = "Queijo")]
        Queijo = 50
    }
}
