﻿using Application.Interfaces;
using Domain.Interfaces.Services;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Applications
{
    public class ItemApplication : IItemApplication
    {
        private readonly IItemService _IItemService;

        public ItemApplication(IItemService IItemService)
        {
            _IItemService = IItemService;
        }


        public async Task AdicionarItem(Item item)
        {
            await _IItemService.AdicionarItem(item);
        }

        public async  Task AtualizarItem(Item item)
        {
            await _IItemService.AtualizarItem(item);
        }

        public async Task<List<Item>> ListarItens(Expression<Func<Item, bool>> expression)
        {
            return await _IItemService.ListarItens(expression);
        }

        public async Task RemoverItem(Item item)
        {
            await _IItemService.RemoverItem(item);
        }
    }
}
