﻿using Application.Interfaces;
using Domain.Interfaces.Services;
using Entities.Entities;
using Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Applications
{
    public class MerceariaApplication : IMerceariaApplication
    {
        private readonly IMerceariaService _IMerceariaService;

        public MerceariaApplication(IMerceariaService IMerceariaService)
        {
            _IMerceariaService = IMerceariaService;
        }

        public async Task AdicionarMercearia(Mercearia mercearia)
        {
            await _IMerceariaService.AdicionarMercearia(mercearia);
        }

        public async Task AtualizarMercearia(Mercearia mercearia)
        {
            await _IMerceariaService.AtualizarMercearia(mercearia);
        }

        public async Task<List<Mercearia>> ListarMercearias(Expression<Func<Mercearia, bool>> expression)
        {
            return await _IMerceariaService.ListarMercearias(expression);
        }
        public async Task<List<Item>> AtualizarItensMercearia(Mercearia mercearia)
        {
            return await _IMerceariaService.AtualizarItensMercearia(mercearia);
        }

        public async Task RemoverMercearia(Mercearia Mercearia)
        {
            await _IMerceariaService.RemoverMercearia(Mercearia);
        }
    }
}
