﻿using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IMerceariaApplication
    {
        Task AdicionarMercearia(Mercearia mercearia);
        Task AtualizarMercearia(Mercearia mercearia);
        Task RemoverMercearia(Mercearia Mercearia);
        Task<List<Mercearia>> ListarMercearias(Expression<Func<Mercearia, bool>> expression);
        Task<List<Item>> AtualizarItensMercearia(Mercearia mercearia);
    }
}
