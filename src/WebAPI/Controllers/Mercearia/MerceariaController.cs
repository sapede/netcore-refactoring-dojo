﻿using Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using Entities.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Entities.Helpers;
using Entities.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerceariaController : ControllerBase
    {
        private readonly IMerceariaApplication _IMerceariaApplication;

        public MerceariaController(IMerceariaApplication IMerceariaApplication)
        {
            _IMerceariaApplication = IMerceariaApplication;
        }


        [Produces("application/json")]
        [HttpPost("/api/ListarMercearias")]
        public async Task<List<Mercearia>> ListarMercearia(MerceariaViewModel vm)
        {
            return await _IMerceariaApplication.ListarMercearias(CriarFiltro(vm));
        }

        [Produces("application/json")]
        [HttpPost("/api/AdicionarMercearia")]
        public async Task<Mercearia> AdicionarMercearia(MerceariaViewModel vm)
        {
            var obj = new Mercearia();
            obj.Nome = vm.Nome;

            await _IMerceariaApplication.AdicionarMercearia(obj);

            return obj;
        }

        [Produces("application/json")]
        [HttpPost("/api/AtualizarQualidade")]
        public async Task<IList<Item>> AtualizarQualidade(Mercearia vm)
        {
            return await _IMerceariaApplication.AtualizarItensMercearia(vm);
        }

        private Expression<Func<Mercearia, bool>> CriarFiltro(MerceariaViewModel vm)
        {
            Expression<Func<Mercearia, bool>> filtro = ent => true;

            if (vm != null)
            {
                Expression<Func<Mercearia, bool>> filtro2 = null;
                if (!string.IsNullOrWhiteSpace(vm.Nome))
                {
                    filtro2 = ent => ent.Nome.Contains(vm.Nome);
                    filtro = filtro.Concat(filtro2);
                }
                if(vm.MerceariaId > 0)
                {
                    filtro2 = ent => ent.MerceariaId == vm.MerceariaId;
                    filtro = filtro.Concat(filtro2);
                }
            }

            return filtro;
        }
    }
}
