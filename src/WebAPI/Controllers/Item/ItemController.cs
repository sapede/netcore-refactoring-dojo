﻿using Application.Interfaces;
using Entities.Entities;
using Entities.Helpers;
using Entities.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IItemApplication _IItemApplication;

        public ItemController(IItemApplication IItemApplication)
        {
            _IItemApplication = IItemApplication;
        }


        [Produces("application/json")]
        [HttpPost("/api/ListarItens")]
        public async Task<List<Item>> ListarItens(ItemViewModel vm)
        {
            var itens = await _IItemApplication.ListarItens(CriarFiltro(vm));
            return itens.ToList();
        }

        [Produces("application/json")]
        [HttpPost("/api/AdicionarItem")]
        public async Task<Item> AdicionarItem(ItemViewModel vm)
        {
            var obj = new Item();
            obj.Nome = vm.Nome;
            obj.Qualidade = vm.Qualidade;
            obj.PrazoValidade = vm.PrazoValidade;
            obj.MerceariaId= vm.MerceariaId;

            await _IItemApplication.AdicionarItem(obj);

            return obj;
        }

        private Expression<Func<Item, bool>> CriarFiltro(ItemViewModel vm)
        {
            Expression<Func<Item, bool>> filtro = ent => true;

            if (vm != null)
            {
                Expression<Func<Item, bool>> filtro2 = null;
                if (!string.IsNullOrWhiteSpace(vm.Nome))
                {
                    filtro2 = ent => ent.Nome.Contains(vm.Nome);
                    filtro = filtro.Concat(filtro2);
                }
                if (vm.PrazoValidade > 0)
                {
                    filtro2 = ent => ent.PrazoValidade == vm.PrazoValidade;
                    filtro = filtro.Concat(filtro2);
                }
                if (vm.Qualidade > 0)
                {
                    filtro2 = ent => ent.Qualidade == vm.Qualidade;
                    filtro = filtro.Concat(filtro2);
                }
            }

            return filtro;
        }
    }
}
