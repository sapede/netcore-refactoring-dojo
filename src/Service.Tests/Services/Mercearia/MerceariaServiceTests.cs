﻿using ApprovalTests;
using Domain.Interfaces.Services;
using Entities.Entities;
using Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;
using Domain.Interfaces.Repository;
using Infra.Repositories;
using Domain.Services;
using ApprovalTests.Reporters;
using System.Linq.Expressions;
using Entities.Enuns;

namespace Application.Tests.Applications
{
    [UseReporter(typeof(DiffReporter))]
    public class MerceariaServiceTests
    {
        private readonly IMerceariaService merceariaApp;
        private readonly IItemService itemApp;

        public MerceariaServiceTests()
        {
            var services = new ServiceCollection();
            services.AddTransient<IMerceariaService, MerceariaService>();
            services.AddTransient<IItemService, ItemService>();
            services.AddTransient<IMerceariaRepository, MerceariaRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();

            var serviceProvider = services.BuildServiceProvider();

            merceariaApp = serviceProvider.GetService<IMerceariaService>();
            itemApp = serviceProvider.GetService<IItemService>();
        }

        [Fact]
        public async void TrintaDias()
        {

            var mercearia = new Mercearia() { Nome = "GildedRose" };

            IList<Item> itens = new List<Item>{
                new Item {Nome = "Corselete +5 DEX", PrazoValidade = 10, Qualidade = 20, TipoItem = TipoItem.Normal },
                new Item {Nome = "Queijo Brie Envelhecido", PrazoValidade = 2, Qualidade = 0, TipoItem = TipoItem.Queijo },
                new Item {Nome = "Elixir do Mangusto", PrazoValidade = 5, Qualidade = 7, MerceariaId = 1, TipoItem = TipoItem.Normal },
                new Item {Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 0, Qualidade = 80, TipoItem = TipoItem.Lendario },
                new Item {Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = -1, Qualidade = 80, TipoItem = TipoItem.Lendario},
                new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 15,
                    Qualidade = 20,
                    TipoItem = TipoItem.Ingressos
                },
                new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 10,
                    Qualidade = 49,
                    TipoItem = TipoItem.Ingressos
                },
                new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 5,
                    Qualidade = 49,
                    TipoItem = TipoItem.Ingressos
                },
                new Item {Nome = "Bolo de Mana Conjurado", PrazoValidade = 3, Qualidade = 6, TipoItem = TipoItem.Conjurados }
            };

            mercearia.Itens = itens;

            await merceariaApp.AdicionarMercearia(mercearia);

            var fakeoutput = new StringBuilder();
            Console.SetOut(new StringWriter(fakeoutput));
            Console.SetIn(new StringReader("a\n"));

            for (var i = 0; i < 31; i++)
            {
                Console.WriteLine("-------- dia " + i + " --------");
                Console.WriteLine("Nome, PrazoValidade, Qualidade");

                foreach (var item in itens)
                {
                    Console.WriteLine(item.Nome + ", " + item.PrazoValidade + ", " + item.Qualidade);
                }
                Console.WriteLine("");

                itens = await merceariaApp.AtualizarItensMercearia(mercearia);

            }

            var output = fakeoutput.ToString();

            Approvals.Verify(output);

            foreach (var item in mercearia.Itens.ToList())
            {
                await itemApp.RemoverItem(item);
            }

            await merceariaApp.RemoverMercearia(mercearia);

        }
    }
}
