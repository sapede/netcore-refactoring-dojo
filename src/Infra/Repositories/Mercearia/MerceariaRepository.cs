﻿using Domain.Interfaces.Repository;
using Entities.Entities;
using Entities.Enuns;
using Infra.Configs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repositories
{
    public class MerceariaRepository : RepositoryGeneric<Mercearia>, IMerceariaRepository
    {
        private readonly DbContextOptions<Context> _OptionsBuilder;
        private readonly IItemRepository _itemRep;

        public MerceariaRepository(IItemRepository itemRep)
        {
            _OptionsBuilder = new DbContextOptions<Context>();
            _itemRep = itemRep;
        }

        public new async Task<List<Mercearia>> Listar(Expression<Func<Mercearia, bool>> expression)
        {
            using (var data = new Context(_OptionsBuilder))
            {
                return await data.Set<Mercearia>().Where(expression).Include(x => x.Itens).AsNoTracking().ToListAsync();
            }
        }

        public async Task<List<Item>> AtualizarItensMercearia(Mercearia mercearia)
        {
            using (var data = new Context(_OptionsBuilder))
            {
                foreach (var item in mercearia.Itens.ToList())
                {
                    await _itemRep.AtualizarQualidade(item);
                }
            }

            return mercearia.Itens.ToList();
        }
    }
}
