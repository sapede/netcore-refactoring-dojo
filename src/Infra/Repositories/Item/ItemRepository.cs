﻿using Domain.Interfaces.Repository;
using Entities.Entities;
using Infra.Configs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repositories
{
    public class ItemRepository : RepositoryGeneric<Item>, IItemRepository
    {
        private readonly DbContextOptions<Context> _OptionsBuilder;

        public ItemRepository()
        {
            _OptionsBuilder = new DbContextOptions<Context>();
        }

        public async Task AtualizarQualidade(Item item)
        {
            using (var data = new Context(_OptionsBuilder))
            {
                AlterarQualidade(item);
                await data.SaveChangesAsync();
            }
        }

        private Item AlterarQualidade(Item item)
        {
            switch (item.TipoItem)
            {
                case Entities.Enuns.TipoItem.Lendario:
                    return item;


                case Entities.Enuns.TipoItem.Normal:
                    {
                        item.PrazoValidade--;
                        if (item.PrazoValidade < 0)
                        {
                            if (item.Qualidade > 0)
                                item.Qualidade-=2;
                        }
                        else
                        {
                            if (item.Qualidade > 0)
                                item.Qualidade--;
                        }
                        item.Qualidade = item.Qualidade >= 0 ? item.Qualidade : 0;
                        return item;
                    };


                case Entities.Enuns.TipoItem.Ingressos:
                    {
                        item.PrazoValidade--;
                        if (item.PrazoValidade < 0)
                        {
                            item.Qualidade = 0;
                        }
                        else
                        {
                            if (item.PrazoValidade >= 10)
                            {
                                item.Qualidade += 1;
                            }
                            else if (item.PrazoValidade < 10 && item.PrazoValidade >= 5)
                            {
                                item.Qualidade += 2;
                            }
                            else if (item.PrazoValidade < 5)
                            {
                                item.Qualidade += 3;
                            }
                            item.Qualidade = item.Qualidade > 50 ? 50 : item.Qualidade;
                        }
                        return item;
                    };


                case Entities.Enuns.TipoItem.Conjurados:
                    {
                        item.PrazoValidade--;
                        if (item.Qualidade > 0)
                            item.Qualidade -= 2;
                        item.Qualidade = item.Qualidade >= 0 ? item.Qualidade : 0;
                        return item;
                    };


                case Entities.Enuns.TipoItem.Queijo:
                    {
                        item.PrazoValidade--;
                        if (item.PrazoValidade >= 0)
                        {
                            item.Qualidade += 1;
                        }
                        else
                        {
                            item.Qualidade += 2;
                        }
                        item.Qualidade = item.Qualidade > 50 ? 50 : item.Qualidade;
                        return item;
                    };

                default:
                    return item;
            }
        }
    }
}
