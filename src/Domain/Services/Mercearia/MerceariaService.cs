﻿using Domain.Interfaces.Repository;
using Domain.Interfaces.Services;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class MerceariaService : IMerceariaService
    {
        private readonly IMerceariaRepository _IMerceariaRepository;

        public MerceariaService(IMerceariaRepository IMerceariaRepository)
        {
            _IMerceariaRepository = IMerceariaRepository;
        }


        public async Task AdicionarMercearia(Mercearia Mercearia)
        {
            await _IMerceariaRepository.Adicionar(Mercearia);
        }

        public async Task AtualizarMercearia(Mercearia Mercearia)
        {
            await _IMerceariaRepository.Atualizar(Mercearia);
        }

        public async Task RemoverMercearia(Mercearia Mercearia)
        {
            await _IMerceariaRepository.Remover(Mercearia);
        }

        public async Task<List<Mercearia>> ListarMercearias(Expression<Func<Mercearia, bool>> expression)
        {
            return await _IMerceariaRepository.Listar(expression);
        }

        public async Task<List<Item>> AtualizarItensMercearia(Mercearia mercearia)
        {
            return await _IMerceariaRepository.AtualizarItensMercearia(mercearia);
        }

    }
}
