﻿using Domain.Interfaces.Repository;
using Domain.Interfaces.Services;
using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository _IItemRepository;

        public ItemService(IItemRepository IItemRepository)
        {
            _IItemRepository = IItemRepository;
        }


        public async Task AdicionarItem(Item item)
        {
            await _IItemRepository.Adicionar(item);
        }

        public async Task AtualizarItem(Item item)
        {
            await _IItemRepository.Atualizar(item);
        }

        public async Task<List<Item>> ListarItens(Expression<Func<Item, bool>> expression)
        {
            return await _IItemRepository.Listar(expression);
        }

        public async Task RemoverItem(Item item)
        {
            await _IItemRepository.Remover(item);
        }
    }
}
