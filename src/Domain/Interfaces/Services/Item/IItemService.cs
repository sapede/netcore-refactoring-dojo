﻿using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Services
{
    public interface IItemService
    {
        Task AdicionarItem(Item item);
        Task AtualizarItem(Item item);
        Task RemoverItem(Item item);
        Task<List<Item>> ListarItens(Expression<Func<Item, bool>> expression);
    }
}
