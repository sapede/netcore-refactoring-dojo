﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repository
{
    public interface IRepositoryGeneric<T> where T : class
    {
        Task Adicionar(T Objeto);
        Task Atualizar(T Objeto);
        Task Remover(T Objeto);
        Task<T> BuscarPorId(int Id);
        Task<List<T>> Listar(Expression<Func<T, bool>> expression);
    }
}
