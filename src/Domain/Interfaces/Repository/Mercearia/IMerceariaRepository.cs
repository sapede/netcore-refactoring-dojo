﻿using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repository
{
    public interface IMerceariaRepository : IRepositoryGeneric<Mercearia>
    {
        public new Task<List<Mercearia>> Listar(Expression<Func<Mercearia, bool>> expression);
        public Task<List<Item>> AtualizarItensMercearia(Mercearia mercearia);
    }
}
