﻿using Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repository
{
    public interface IItemRepository :  IRepositoryGeneric<Item>
    {
        Task AtualizarQualidade(Item item);
    }
}
